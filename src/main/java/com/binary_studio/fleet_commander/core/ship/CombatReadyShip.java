package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {
    private String name;
    private DockedShip ship;
    private AttackSubsystem attackSubsystem;
    private DefenciveSubsystem defenciveSubsystem;
    private PositiveInteger shieldHP;
    private PositiveInteger hullHP;
    private PositiveInteger capacitor;
    private PositiveInteger capacitorRegeneration;
    private PositiveInteger pg;
    private PositiveInteger size;
    private PositiveInteger speed;
    private int currentHullHP;
    private int currentShieldHP;
    private int currentPower;


    public CombatReadyShip(DockedShip dockedShip) {
        this.name = dockedShip.getName();
        this.ship = dockedShip;
        this.attackSubsystem = dockedShip.getAttackSubsystem();
        this.defenciveSubsystem = dockedShip.getDefenciveSubsystem();
        this.shieldHP = dockedShip.getShieldHP();
        this.hullHP = dockedShip.getHullHP();
        this.capacitor = dockedShip.getCapacitor();
        this.capacitorRegeneration = dockedShip.getCapacitorRegeneration();
        this.pg = dockedShip.getPg();
        this.currentHullHP = hullHP.value();
        this.currentShieldHP = shieldHP.value();
        this.currentPower = dockedShip.getCapacitor().value();

    }

    public void setSize(PositiveInteger size) {
        this.size = size;
    }

    public void setSpeed(PositiveInteger speed) {
        this.speed = speed;
    }

    @Override
    public void endTurn() {
		currentPower = Math.min(
				capacitorRegeneration.value() + currentPower, capacitor.value());
    }

    @Override
    public void startTurn() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public PositiveInteger getSize() {
        return size;
    }

    @Override
    public PositiveInteger getCurrentSpeed() {
        return speed;
    }

    @Override
    public Optional<AttackAction> attack(Attackable target) {
		if (capacitor.value()
				- attackSubsystem.getPowerGridConsumption().value() < 0) {
			return Optional.empty();
		} else {
			currentPower = capacitor.value()
					- attackSubsystem.getPowerGridConsumption().value();
			return Optional.of(new AttackAction(attackSubsystem.getBaseDamage(), this, target, attackSubsystem));
		}
	}

    @Override
    public AttackResult applyAttack(AttackAction attack) {
        AttackAction attackResult = defenciveSubsystem.reduceDamage(attack);
        int actualDamage = attackResult.damage.value();
        if (currentShieldHP > 0) {
            if (actualDamage > currentShieldHP) {
                currentShieldHP = 0;
                if (actualDamage >= currentHullHP) {
                    return new AttackResult.Destroyed();
                } else {
                    currentHullHP = currentHullHP - actualDamage;
                    return new AttackResult.DamageRecived(attackResult.weapon, attackResult.damage, this);
                }
            } else {
                currentShieldHP = currentShieldHP - actualDamage;
                return new AttackResult.DamageRecived(attackResult.weapon, attackResult.damage, this);
            }
        } else {
            if (actualDamage >= currentHullHP) {
                return new AttackResult.Destroyed();
            } else {
                currentHullHP = currentHullHP - actualDamage;
                return new AttackResult.DamageRecived(attackResult.weapon, attackResult.damage, this);
            }
        }
    }

    @Override
    public Optional<RegenerateAction> regenerate() {
        if (currentPower
                - defenciveSubsystem.getCapacitorConsumption().value() < 0) {
            return Optional.empty();
        }
        currentShieldHP = Math.min(defenciveSubsystem.regenerate().shieldHPRegenerated.value(), shieldHP.value());
        currentHullHP = Math.min(defenciveSubsystem.regenerate().hullHPRegenerated.value(), hullHP.value());
        return Optional.of(new RegenerateAction(PositiveInteger.of(currentShieldHP),
                PositiveInteger.of(currentHullHP)));
    }
}


