package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {
    private DockedShipBuilder dockedShipBuilder;
    private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;

	public AttackSubsystem getAttackSubsystem() {
		return attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return defenciveSubsystem;
	}

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
					   PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate) {
        dockedShipBuilder = DockedShipBuilder.named(name)
                .shield(shieldHP.value())
                .hull(hullHP.value())
                .pg(powergridOutput.value())
                .capacitor(capacitorAmount.value())
                .capacitorRegen(capacitorRechargeRate.value());
    }

    public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                                       PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate) {
        return new DockedShip(name,shieldHP,hullHP,powergridOutput,capacitorAmount,capacitorRechargeRate);
    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
    	if (subsystem == null){
    		attackSubsystem = null;
		}else if(getPg().value() >= subsystem.getPowerGridConsumption().value()) {
			attackSubsystem = subsystem;
			setPg(new PositiveInteger(getPg().value()-subsystem.getPowerGridConsumption().value()));
		}
		else
			throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value());
    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null){
			defenciveSubsystem = null;
		}else if(getPg().value() >= subsystem.getPowerGridConsumption().value()) {
			defenciveSubsystem = subsystem;
			setPg(new PositiveInteger(getPg().value()-subsystem.getPowerGridConsumption().value()));
		}
		else
			throw new InsufficientPowergridException(subsystem.getPowerGridConsumption().value());

    }

    public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if(attackSubsystem == null && defenciveSubsystem == null) throw NotAllSubsystemsFitted.bothMissing();
		else if(attackSubsystem == null) throw NotAllSubsystemsFitted.attackMissing();
		else if(defenciveSubsystem == null) throw NotAllSubsystemsFitted.defenciveMissing();
		return new CombatReadyShip(this);
    }

	public PositiveInteger getShieldHP() {
		return dockedShipBuilder.getShieldHP();
	}

	public PositiveInteger getHullHP() {
		return dockedShipBuilder.getHullHP();
	}

	public PositiveInteger getCapacitor() {
		return dockedShipBuilder.getCapacitor();
	}

	public PositiveInteger getCapacitorRegeneration() {
		return dockedShipBuilder.getCapacitorRegeneration();
	}

	public PositiveInteger getPg() {
		return dockedShipBuilder.getPg();
	}

	public void setPg(PositiveInteger pg) {
		dockedShipBuilder.setPg(pg);
	}

	public String getName(){
    	return dockedShipBuilder.getName();
	}


}
