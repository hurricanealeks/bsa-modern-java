package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	private DefenciveSubsystemBuilder defenciveSubsystemBuilder;

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption,
								  PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration,
								  PositiveInteger hullRegeneration){
		defenciveSubsystemBuilder = DefenciveSubsystemBuilder.named(name)
				.pg(powergridConsumption.value())
				.capacitorUsage(capacitorConsumption.value())
				.impactReduction(impactReductionPercent.value())
				.shieldRegen(shieldRegeneration.value())
				.hullRegen(hullRegeneration.value());
	}
	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
												   PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
												   PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if(name.trim().isEmpty()) throw new IllegalArgumentException("Name should be not null and not empty");
		return new DefenciveSubsystemImpl(name,powergridConsumption,capacitorConsumption,impactReductionPercent,
				shieldRegeneration,hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return defenciveSubsystemBuilder.getPgRequirement();
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return defenciveSubsystemBuilder.getCapacitorUsage();
	}

	@Override
	public String getName() {
		return defenciveSubsystemBuilder.getName();
	}

	public PositiveInteger getImpactReduction() {
		return defenciveSubsystemBuilder.getImpactReduction();
	}

	public PositiveInteger getShieldRegen() {
		return defenciveSubsystemBuilder.getShieldRegen();
	}

	public PositiveInteger getHullRegen() {
		return defenciveSubsystemBuilder.getHullRegen();
	}


	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int damageRatio;
		int actualDamage;

		if (getImpactReduction().value() > 95){
			damageRatio = 95;
		}else {
			damageRatio = getImpactReduction().value();
		}

		actualDamage = (int)Math.ceil(incomingDamage.damage.value() - 1.0*incomingDamage.damage.value()*damageRatio/100);

		if (actualDamage == 0){
			actualDamage = 1;
		}

		return new AttackAction(PositiveInteger.of(actualDamage), incomingDamage.attacker, incomingDamage.target,incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(getShieldRegen(),getHullRegen());
	}

}
