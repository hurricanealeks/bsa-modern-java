package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
    private AttackSubsystemBuilder attackSubsystemBuilder;

    private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments,
                               PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                               PositiveInteger baseDamage) {
        attackSubsystemBuilder = AttackSubsystemBuilder.named(name)
                .pg(powergridRequirments.value())
                .capacitorUsage(capacitorConsumption.value())
                .optimalSpeed(optimalSpeed.value())
                .optimalSize(optimalSize.value())
                .damage(baseDamage.value());
    }

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {
        if (name.trim().isEmpty()) {
            throw new IllegalArgumentException("Name should be not null and not empty");
        }

        return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
                baseDamage);
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return attackSubsystemBuilder.getPgRequirement();
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return attackSubsystemBuilder.getCapacitorUsage();
    }

    @Override
    public PositiveInteger attack(Attackable target) {
        double sizeReductionModifier;
        double speedReductionModifier;
        double damage;

        if (target.getSize().value() >= getSize().value()) {
            sizeReductionModifier = 1;
        } else {
            sizeReductionModifier = Double.valueOf(target.getSize().value()) / this.getSize().value();
        }

        if (target.getCurrentSpeed().value() <= getSpeed().value()) {
            speedReductionModifier = 1;
        } else {
            speedReductionModifier = Double.valueOf(getSpeed().value()) / (double) (2 * target.getCurrentSpeed().value());
        }

		damage = getBaseDamage().value() * Double.min(sizeReductionModifier, speedReductionModifier);

        if (damage == 0){
        	return new PositiveInteger(1);
		}

        return new PositiveInteger((int) Math.ceil(damage));
    }

    @Override
    public String getName() {
        return attackSubsystemBuilder.getName();
    }

    public PositiveInteger getSize() {
        return attackSubsystemBuilder.getOptimalSize();
    }

    public PositiveInteger getSpeed() {
        return attackSubsystemBuilder.getOptimalSpeed();
    }

	public PositiveInteger getBaseDamage() {
		return attackSubsystemBuilder.getBaseDamage();
	}

}
