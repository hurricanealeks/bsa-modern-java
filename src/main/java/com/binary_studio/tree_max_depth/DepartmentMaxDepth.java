package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		int maxDepth = 1;

		if (rootDepartment == null){
			return 0;
		}

		for (Department d :
				rootDepartment.subDepartments) {
			int tmp = calculateMaxDepth(d) + 1;
			if (tmp > maxDepth){
				maxDepth = tmp;
			}
		}
		return maxDepth;
	}

}
